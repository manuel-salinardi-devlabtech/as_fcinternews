package org.azasoft.FcInterNews.transform;

import org.simpleframework.xml.transform.Matcher;
import org.simpleframework.xml.transform.Transform;

/**
 * Created by dario on 23/09/16.
 */

public class JodaTimeDateTimeMatcher implements Matcher {
    @Override
    public Transform match(Class type) throws Exception {
        // Is DateTime a superclass (or same class) the classType?
        if ( org.joda.time.DateTime.class.isAssignableFrom( type ) ) {
            return new JodaTimeDateTimeTransform();
        }
        return null;
    }
}

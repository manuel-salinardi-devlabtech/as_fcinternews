package org.azasoft.FcInterNews.transform;

import org.joda.time.DateTime;
import org.simpleframework.xml.transform.Transform;

/**
 * Created by dario on 23/09/16.
 */

public class JodaTimeDateTimeTransform implements Transform<DateTime> {

    @Override
    public DateTime read(String value) throws Exception {
        DateTime dateTime = null;
        try {
            dateTime = new DateTime( value );  // Keeping whatever offset is included. Not forcing to UTC.
        } catch ( Exception e ) {
            //logger.debug( "Joda-Time DateTime Transform failed. Exception: " + e );
        }
        return dateTime;
    }

    @Override
    public String write(DateTime value) throws Exception {
        String output = value.toString();  // Keeping whatever offset is included. Not forcing to UTC.
        return output;
    }
}

package org.azasoft.FcInterNews;

import org.azasoft.FcInterNews.RSS.Article;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by dario on 22/08/16.
 */
@Root
public class tcc {
    @ElementList
    public ArrayList<Article> articles;
}

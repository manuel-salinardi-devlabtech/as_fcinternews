package org.azasoft.FcInterNews;

import android.app.Application;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.multidex.MultiDex;
import com.google.android.gms.ads.MobileAds;
import com.jointag.proximity.ProximitySDK;
import com.onesignal.OneSignal;

import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.OneSignal.NotificationOpenedHandler;
import org.azasoft.FcInterNews.OneSignal.NotificationReceivedHandler;

public class App extends Application {

    private static Context appContext;
    private static Category selectedCategory;

    public static String linkFromPushNotification;
    public static boolean isFromPushNotification;

    public static boolean isPlayingADS = false;

    public enum CONSTANT {
        TITLE,
        LINK,
        MAIN_LINK,
        IS_FROM_NOTIFICATION,
        SHOW_INTERSTITIAL,
        NOTIFICATION
    }

    public static Category getSelectedCategory() {
        return selectedCategory;
    }

    public static void setSelectedCategory(Category selectedCategory) {
        App.selectedCategory = selectedCategory;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;

        MobileAds.initialize(this, getResources().getString(R.string.admob_app_id));

        ProximitySDK.init(this, "5aab90cb675e9f3f6b1ebce4", "585a598308cae16c9de6ca3140265208b39000be3c1a3c61");

        OneSignal.startInit(this)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public static Context getContext(){
        return appContext;
    }
}

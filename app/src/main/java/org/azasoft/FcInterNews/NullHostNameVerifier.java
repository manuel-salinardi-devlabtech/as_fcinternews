package org.azasoft.FcInterNews;

import android.util.Log;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class NullHostNameVerifier implements HostnameVerifier {

    @Override
    public boolean verify(String hostname, SSLSession session) {
        Log.i("RestUtilImpl", "Approving certificate for " + hostname);

        if (hostname.toLowerCase().contains("tccapis.com") || !(hostname.toLowerCase().contains("tccapis.com")))
            return true;
        else
            return false;
    }

}
package org.azasoft.FcInterNews.RSS;

import org.joda.time.DateTime;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by dario on 22/08/16.
 */
@Root
public class Article {
    @Element
    String id;
    @Element
    DateTime date;
    @Element
    String title;
    @Element
    String section;
    @Element(required = false)
    String text;
    @Element(required = false)
    String author;
    @Element(required = false)
    String source;
    @Element(required = false)
    String url;
    @Element(required = false)
    String thumb1;
    @Element(required = false)
    String thumb2;
    @Element(required = false)
    Media media;
    @Element(required = false)
    String summary;
    @ElementList(required = false)
    ArrayList<Link> links;
}
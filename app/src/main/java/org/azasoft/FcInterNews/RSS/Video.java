package org.azasoft.FcInterNews.RSS;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by dario on 24/08/16.
 */
@Root
public class Video {
    @Attribute
    String id;
    @Attribute
    String source;
    @Attribute
    String url;
    @Attribute
    String thumb;
}

package org.azasoft.FcInterNews.RSS;

import android.util.Log;

import org.azasoft.FcInterNews.Models.Detail;
import org.azasoft.FcInterNews.Utility;
import org.azasoft.FcInterNews.tcc;
import org.azasoft.FcInterNews.transform.JodaTimeDateTimeTransform;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class Parser {
	private static Set<String> idUniqueList = new HashSet<>();


	public static RSSFeed ParseLista(String urlSezione, String sezione, int start, int limit) {
		RSSFeed ret = new RSSFeed();

		
		//TODO: Estrarre dalla URL sezione il numero della sezione da usare nella url
		int section = 0;
		int startSection = urlSezione.indexOf("c=");
		if (startSection > -1) {
			section = Integer.parseInt(urlSezione.substring(startSection + 2));
		}

		StringBuilder str = new StringBuilder(); 
		URL aURL;
		URL url;
		try {
			Utility.EnableSSL();

			//chiamare il servizio web per la lista degli elementi e popolare RSSFeed
			String uuid = UUID.randomUUID().toString();

			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());

			String restUrl = "https://fcin-android-d9f3.articles-pub.v1.tccapis.com/?start={start}&step={step}";

			ArrayList<Article> articles = new ArrayList<>();

			if (start == 0) idUniqueList.clear();

			// if is homepage: http://feeds.fcinternews.it/rss
			if (startSection == -1 && start == 0) {
				// get first 3 articles of In Primo Piano, first 2 of Focus and 15 articles of all news
				ArrayList<Article> ArticlesInPrimoPiano = fillArticlesFromHTTP(start, 3, uuid, restTemplate, restUrl, 15);
				ArrayList<Article> ArticlesFocus = fillArticlesFromHTTP(start, 2, uuid, restTemplate, restUrl, 38);
				ArrayList<Article> ArticlesAllNews = fillArticlesFromHTTP(start, limit, uuid, restTemplate, restUrl, 0);

				// create one arraylist with the above three arraylist
				articles.addAll(ArticlesInPrimoPiano);
				articles.addAll(ArticlesFocus);
				articles.addAll(ArticlesAllNews);
			} else {
				articles = fillArticlesFromHTTP(start, limit, uuid, restTemplate, restUrl, section);
			}

			Locale.setDefault(Locale.ITALIAN);
			DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd MMMM yyyy kk:mm:ss");


			int value = 8;
            for(int i = 0; i < articles.size(); i++) {
				Article a = articles.get(i);

				// check if the article is already inside the recyclerview
				if (a.id != null) {
					boolean isNotPresentYet = idUniqueList.add(a.id);
					if (!isNotPresentYet)
						continue;
				}

                int index = articles.indexOf(a);

                if (index != 0 && index % value == 0) {
                    value = 20;
                    RSSItem item = new RSSItem();
                    item.setIsAdView(true);
                    ret.addItem(item);
                }

                RSSItem item = new RSSItem();

				if (startSection == -1) {
					if (index < 5 && start == 0) {
						item.setIsBigNews(true);
					}
				} else {
					if (index == 0 && start == 0) {
						item.setIsBigNews(true);
					}
				}
                item.setTitle(a.title);
                item.setSection(section);
				String bigThumb1 = a.thumb2.replace("/thumb2/","/thumb1/");

				item.setEnclosure(bigThumb1);
				//item.setEnclosure(a.thumb2);
                item.setPubDate(dtfOut.print(a.date));
                item.setCategory(a.section);
                item.setLink(a.id);

                // set the big image
				String bigThumb3 = a.thumb2.replace("/thumb2/","/thumb3/");
				item.setEnclosureBig(bigThumb3);

                ret.addItem(item);
            }
		} catch (Exception e) {
			Log.e("ParserLista", urlSezione, e); 
		} 
		
		return ret;
	}

	private static ArrayList<Article> fillArticlesFromHTTP(int start, int limit,
											String uuid, RestTemplate restTemplate, String restUrl,
											int section) throws Exception {
		StringBuilder params = new StringBuilder();

		params.append(String.format("%s=%s&", "start", String.format("%1$d", start)));
		params.append(String.format("%s=%s", "step", String.format("%1$d", limit)));

		String par = "";
		if (section > 0) {
			par = "fcinternews.it-" + String.format("%1$d", section);
			params.append(String.format("&%s=%s", "s", par));
			restUrl += "&s=" + par;
		}

		String hexString = Utility.getHash(params, uuid);

		HttpHeaders headers = new HttpHeaders();

		headers.add("X-TCC-UUID", uuid);
		headers.add("X-TCC-Secret", hexString);
		headers.add("X-TCC-Version", "1.1");

		HttpEntity entity = new HttpEntity(headers);

		ResponseEntity<String> response = restTemplate.exchange(restUrl, HttpMethod.GET, entity, String.class, String.format("%1$d", start), String.format("%1$d", limit));
		String resultString = response.getBody();

		RegistryMatcher matchers = new RegistryMatcher();
		matchers.bind( org.joda.time.DateTime.class , JodaTimeDateTimeTransform.class );

		Strategy strategy = new AnnotationStrategy();
		Serializer serializer = new Persister( strategy , matchers );

		tcc result = serializer.read(tcc.class, resultString);

		return result.articles;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Detail Parse(String id)
	{
		Detail dett = new Detail();

		try {
			//chiamare il servizio web per avere il Detail dell'articolo e popolare l'istanza di Detail con i dati.
			Utility.EnableSSL();

			String uuid = UUID.randomUUID().toString();

			RestTemplate restTemplate = new RestTemplate();

			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

			SimpleXmlHttpMessageConverter xmlConverter = new SimpleXmlHttpMessageConverter();

			restTemplate.getMessageConverters().add(xmlConverter);

			String restUrl = "https://fcin-android-d9f3.articles-pub.v1.tccapis.com/?id={id}";

			StringBuilder params = new StringBuilder();

			params.append(String.format("%s=%s", "id", id));

			String hexString = Utility.getHash(params, uuid);

			HttpHeaders headers = new HttpHeaders();

			headers.add("X-TCC-UUID", uuid);
			headers.add("X-TCC-Secret", hexString);
			headers.add("X-TCC-Version", "1.1");

			ArrayList<Charset> charsets = new ArrayList<Charset>();
			charsets.add(Charset.forName("UTF-8"));
			headers.setAcceptCharset(charsets);

			HttpEntity entity = new HttpEntity(headers);

			ResponseEntity<String> response = restTemplate.exchange(restUrl, HttpMethod.GET, entity, String.class, id);
			String resultString = response.getBody();

			RegistryMatcher matchers = new RegistryMatcher();
			matchers.bind(org.joda.time.DateTime.class, JodaTimeDateTimeTransform.class);

			Strategy strategy = new AnnotationStrategy();
			Serializer serializer = new Persister(strategy, matchers);

			tcc tccResult = serializer.read(tcc.class, resultString);

			Article result = tccResult.articles.get(0);

			dett.setCategory(result.section);
			dett.setAuthor(result.author);
			dett.setText(result.text);
			dett.setTitle(result.title);

			String bigThumb = result.thumb1.replace("/thumb1/","/thumb3/");
			dett.setImage(bigThumb);

			Detail.setShareUrl(result.url);

			Locale.setDefault(Locale.ITALIAN);
			DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd-MM-yyyy kk:mm:ss");

			dett.setDate(dtfOut.print(result.date));

			if (result.media != null && result.media.video != null && !result.media.video.isEmpty()) {
				ArrayList<YoutubeLink> list = new ArrayList<YoutubeLink>();
				for (int i = 0; i < result.media.video.size(); i++) {
					if (result.media.video.get(i).url.length() > 0) {
						YoutubeLink link = new YoutubeLink("Guarda il video su YouTube", result.media.video.get(i).url);
						list.add(link);
					}
				}


				dett.setLinkYoutube(list);
			}
			if (result.links != null && !result.links.isEmpty() && result.links.get(0).url.length() > 0) {
				ArrayList<YoutubeLink> list = new ArrayList<YoutubeLink>();
				for (int i = 0; i < result.links.size(); i++) {
					YoutubeLink link = new YoutubeLink(result.links.get(i).value, result.links.get(i).url);
					list.add(link);
				}
				dett.setLinkYoutube(list);
			}
		} catch (Exception e) {
			int i = 0;
			i++;

		}

		return dett;
	}
}

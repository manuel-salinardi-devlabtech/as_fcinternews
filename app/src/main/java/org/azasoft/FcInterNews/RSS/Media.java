package org.azasoft.FcInterNews.RSS;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by dario on 24/08/16.
 */
@Root
public class Media {
    @ElementList(required = false, inline = true)
    ArrayList<Video> video;
    @ElementList(required = false, inline = true)
    ArrayList<Photo> photo;
}

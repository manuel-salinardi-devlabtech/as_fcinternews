package org.azasoft.FcInterNews.RSS;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Photo {
    @Attribute
    String thumb1;
    @Attribute
    String thumb2;
    @Attribute
    String author;
    @Attribute
    String description;
}

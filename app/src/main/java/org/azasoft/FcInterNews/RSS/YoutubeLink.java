package org.azasoft.FcInterNews.RSS;

public class YoutubeLink
{
	private String _testo;
	private String _link;
	
	public YoutubeLink(String testo, String link)
	{
		this._testo=testo;
		this._link=link;
	}
	
	public String getTesto()
	{
		return _testo;
	}
	public void setTesto(String testo)
	{
		this._testo=testo;
	}
	public String getLink()
	{
		return _link;
	}
	public void setLink(String link)
	{
		this._link=link;
	}
}
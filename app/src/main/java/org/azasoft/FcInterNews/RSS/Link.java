package org.azasoft.FcInterNews.RSS;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Created by dario on 28/10/16.
 */

@Root
public class Link {
    @Attribute
    String url;

    @Text
    public String value;
}

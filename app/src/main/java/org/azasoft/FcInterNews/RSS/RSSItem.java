package org.azasoft.FcInterNews.RSS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.graphics.Bitmap;



public class RSSItem
{
    private String _title = null;
    private String _description = null;
    private String _link = null;
    private String _category = null;
    private String _pubdate = null;
    private String _enclosure = null;
    private String _enclosureBig = null;
    private Date _pubdateData=null;
    private Bitmap _immagine =null;
    private Boolean _isAdView = false;
    private Boolean _isBigNews = false;
    private int _section;

    RSSItem() { }

    void setTitle(String title) { _title = title; }
    void setDescription(String description) { _description = description; }
    void setLink(String link) { _link = link; }
    void setCategory(String category) { _category = category; }
    void setPubDate(String pubdate) {
        _pubdate = pubdate;
        _pubdateData=ConvertToDate(_pubdate);
    }
    void setEnclosure(String enclosure) { _enclosure=enclosure; }
    void setEnclosureBig(String enclosureBig) { _enclosureBig=enclosureBig; }
    public void setImmagine(Bitmap immagine) { _immagine=immagine; }
    void setIsAdView(Boolean isAdView) { _isAdView = isAdView; }

    public String getTitle() { return _title; }
    public String getDescription()
    {
        if (_description == null)
            return "";

        if (_description.length() > 100)
        {
            return _description.substring(0, 100) + "...";
        }
        return _description;
    }
    public String getLink() { return _link; }
    public String getCategory() { return _category; }
    public String getPubDate()
    {
        if (_pubdateData != null)
        {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return formatter.format(_pubdateData);
        }
        else
        {
            return _pubdate;
        }

    }
    public String getEnclosure() {
        return _enclosure;
    }
    public String getEnclosureBig() {
        return _enclosureBig;
    }
    public Bitmap getImmagine()
    {
        return _immagine;
    }

    private Date ConvertToDate(String dateString){

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z",Locale.US);
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
        return convertedDate;
    }

    public Boolean getIsAdView() { return _isAdView; }

    public void setSection(int value) { this._section = value; }
    public int getSection() { return this._section; }

    public void setIsBigNews(boolean value) {
        this._isBigNews = value;
    }

    public boolean getIsBigNews() {
        return this._isBigNews;
    }
}

package org.azasoft.FcInterNews.Activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.Fragments.NewsDetailFragment;
import org.azasoft.FcInterNews.Models.Detail;
import org.azasoft.FcInterNews.R;

import java.util.Objects;

public class NewsDetailActivity extends AppCompatActivity {

    private static TypedArray backgroundArray;
    public static int BACKGROUND_COLOR_APPTHEME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_news_detail);

        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_news_detail);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");

        // set the button up
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        backgroundArray = getTheme().obtainStyledAttributes(R.style.AppTheme, new int[] {R.attr.colorBackgroundFloating});
        BACKGROUND_COLOR_APPTHEME = backgroundArray.getResourceId(0, 0);
    }

    // ---------------------- SET MENU ACTIONS ( only share icon here ) ------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_detail_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);

        if (App.isFromPushNotification) {
            App.isFromPushNotification = false;
            menu.findItem(R.id.toolbar_left_arrow).setVisible(false);
            menu.findItem(R.id.toolbar_right_arrow).setVisible(false);
        } else {
            menu.findItem(R.id.toolbar_left_arrow).setVisible(true);
            menu.findItem(R.id.toolbar_right_arrow).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    // ---------------------- CLICK ON MUNU ITEMS ( settings icon ) ------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NewsDetailFragment frag = (NewsDetailFragment)
                getSupportFragmentManager().findFragmentById(R.id.news_detail_fragment);

        switch (item.getItemId()) {
            case R.id.toolbar_left_arrow: frag.prevNews(); return true;
            case R.id.toolbar_right_arrow: frag.nextNews(); return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

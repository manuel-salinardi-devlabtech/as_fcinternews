package org.azasoft.FcInterNews.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = new Intent();
        i.setClass(getApplicationContext(), HomePageActivity.class);
        startActivity(i);
        finish();
    }}

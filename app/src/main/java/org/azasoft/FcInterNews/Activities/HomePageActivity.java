package org.azasoft.FcInterNews.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.onesignal.OSPermissionObserver;
import com.onesignal.OSPermissionStateChanges;
import com.onesignal.OneSignal;
import com.smartadserver.android.library.SASBannerView;
import com.smartadserver.android.library.model.SASAdElement;
import com.smartadserver.android.library.ui.SASAdView;

import org.azasoft.FcInterNews.Fragments.NewsListFragment;
import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.Models.Categories;
import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.SettingsTabMenu.SettingsTabMenuUtility;
import org.azasoft.FcInterNews.Utility;

import java.util.ArrayList;
import java.util.HashMap;

public class HomePageActivity
        extends
            AppCompatActivity
        implements
            OSPermissionObserver,
            NavigationView.OnNavigationItemSelectedListener,
            SwipeRefreshLayout.OnRefreshListener {

    // ------------------------ ----------------
    // ------------ PROPRIETIES ----------------
    // -----------------------------------------

    private Activity self;
    private NewsListFragment newsListFragment;

    // categories
    private ArrayList<Category> listCategories = new ArrayList<>();
    private final ArrayList<Category> listCategoriesComplete = (ArrayList<Category>) Categories.LIST_CATEGORIES;
    private final HashMap<Integer, Category> hashMapCategories =  Categories.HASH_CATEGORIES;

    // ads
    private boolean toInitialize = true;
    private SASBannerView adInlineAdView_320_50;
    private String shouldShowRequestPermission = "shouldShowRequestPermission";
    private TabLayout.OnTabSelectedListener tabSelectedListener;

    // -----------------------------------------
    // ------------ ON CREATE   ----------------
    // -----------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        self = this;
        Bundle bundle = new Bundle();
        final SharedPreferences sharedPrefNotifications = this.getSharedPreferences(this.getString(R.string.fcinternews_preference_file_key), Context.MODE_PRIVATE);

        setLinkAndTitle(bundle);
        setRecyclerView(bundle);
        setFloatingActionButton();
        setNavigationDrawer(setToolbar());
        setSwipeRefreshLayout();
        setPushNotification(sharedPrefNotifications);
        setJoinTag(sharedPrefNotifications);
    }

    // -----------------------------------------
    // ------------ ON START    ----------------
    // -----------------------------------------
    @Override
    protected void onStart() {
        super.onStart();
        openFromNotification();
    }

    // -----------------------------------------
    // ------------ ON RESUME   ----------------
    // -----------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        resetTabList();
        setTabLayout();
    }

    // -----------------------------------------
    // ------------ ON PAUSE    ----------------
    // -----------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        // ---------------------- REMOVE CLICK LISTENER  ---------------------
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.removeOnTabSelectedListener(tabSelectedListener);
    }

    // -----------------------------------------
    // ------------ ON DESTROY  ----------------
    // -----------------------------------------
    @Override
    protected void onDestroy() {
        super.onDestroy();
        adInlineAdView_320_50.onDestroy();
    }

    // ---------------------- SET MENU ACTIONS ( only settings icon here ) ------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the app bar.
        getMenuInflater().inflate(R.menu.homepage_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // ---------------------- CLICK ON MUNU ITEMS ( settings icon ) ------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_customize_menu: {
                Intent intent = new Intent(this, SettingsTabMenuActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.toolbar_customize_notifications: {
                Intent intent = new Intent(this, SettingsPushActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // ---------------------- CLICK ON NAVIGATION DRAWLER ---------------------
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        String navigationDrawlerTitle = (String) item.getTitle();
        for (Category category : listCategoriesComplete) {
            if (category.getTitle().equals(navigationDrawlerTitle)) {
                // open link in the browser, like store online
                if (category.isForBrowser()) {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(category.getLink()));
                    startActivity(myIntent);
                    closeDrawer();
                    return true;
                    //  fill recyclerview with the rights data and select the item into tab menu
                } else {
                    refillRecyclerView(hashMapCategories.get(category.getId()));
                    SettingsTabMenuUtility.selectItemOnTabMenu(category, self, null);
                    break;
                }
            }
        }
        closeDrawer();
        return true;
    }

    // ---------------------- ON SCROLL DOWN RECYCLERVIEW ---------------------
    @Override
    public void onRefresh() {
        NewsListFragment newsListFragment = (NewsListFragment)getSupportFragmentManager().findFragmentById(R.id.newsListFragment);
        if (newsListFragment != null) {
            // Make new fragment to show this selection.
            newsListFragment.refresh(null);
        }
    }

    // -----------------------------------------------------
    // -------------- METHODS ------------------------------
    // -----------------------------------------------------

    private void setLinkAndTitle(Bundle bundle) {
        // if the category is not set yet fill bundle with all news link and title
        if (App.getSelectedCategory() == null) {
            bundle.putString(App.CONSTANT.TITLE.toString(), getResources().getString(R.string.tab_all_news));
            bundle.putString(App.CONSTANT.LINK.toString(), getResources().getString(R.string.link_all_news));
        } else {
            bundle.putString(App.CONSTANT.TITLE.toString(), App.getSelectedCategory().getLink());
            bundle.putString(App.CONSTANT.LINK.toString(), App.getSelectedCategory().getLink());
        }
    }

    private void setRecyclerView(Bundle bundle) {
        newsListFragment = (NewsListFragment) getSupportFragmentManager().findFragmentById(R.id.newsListFragment);
        newsListFragment.setHomePageActivity(self);
        newsListFragment.setLinkAndRefresh(bundle);
    }

    private void setFloatingActionButton() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsListFragment.scrollToFirstItem();
            }
        });
    }

    private Toolbar setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        return toolbar;
    }

    private void setNavigationDrawer(Toolbar toolbar) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);


        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setSwipeRefreshLayout() {
        SwipeRefreshLayout mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }
    
    private void setPushNotification(final SharedPreferences sharedPrefNotifications) {
        boolean setting_notification = sharedPrefNotifications.getBoolean(this.getString(R.string.pref_key_notification), true);
        Utility.PutPreference(this, R.string.pref_key_notification, setting_notification, R.string.fcinternews_preference_file_key);

        if (!setting_notification) {
            int counter_notification = Utility.GetPreference(this, R.string.saved_counter_notification, this.getResources().getInteger(R.integer.notification_counter_default), R.string.fcinternews_preference_file_key);
            if (counter_notification <= 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setInverseBackgroundForced(true);
                builder.setTitle(this.getString(R.string.alert_notification_title));
                builder.setMessage(this.getString(R.string.alert_notification));

                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent s = new Intent();
                        startActivity(s);
                        Utility.PutPreference(getApplicationContext(), R.string.pref_key_notification, true, R.string.fcinternews_preference_file_key);
                    }
                });

                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Utility.PutPreference(getApplicationContext(), R.string.saved_counter_notification, getApplicationContext().getResources().getInteger(R.integer.notification_counter_default), R.string.fcinternews_preference_file_key);
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void setJoinTag(final SharedPreferences sharedPrefNotifications) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                if (!sharedPrefNotifications.contains(shouldShowRequestPermission) || sharedPrefNotifications.getBoolean(shouldShowRequestPermission, true)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.jointag_dialog_message)
                            .setTitle(R.string.jointag_dialog_title);

                    builder.setPositiveButton(R.string.jointag_dialog_ok_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            ActivityCompat.requestPermissions(self,
                                    new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                                    0);
                        }
                    });

                    builder.setNegativeButton(R.string.jointag_dialog_cancel_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked Cancel button
                            sharedPrefNotifications.edit().putBoolean(shouldShowRequestPermission, false).commit();
                        }
                    });

                    builder.setNeutralButton(R.string.jointag_dialog_info_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.fcinternews.it/privacy"));
                            startActivity(browserIntent);
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                        0);
            }
        }
        createElements();
    }
    
    private void openFromNotification() {
        // reset badge count notification
        OneSignal.clearOneSignalNotifications();

        // ------------- FROM CLICK ON PUSH NOTIFICATIONS -------------
        if (App.linkFromPushNotification != null) {
            Bundle bundle = new Bundle();

            // get id link from push
            String id = getIdFromPushNotification();
            fillBundleFromNotification(id, bundle);

            // start newsDetailActivity
            Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
            intent.putExtra("android.intent.extra.INTENT", bundle);
            startActivity(intent);
        }
    }

    private String getIdFromPushNotification() {
        String[] spliced = App.linkFromPushNotification.split("-");
        App.linkFromPushNotification = null;
        return spliced[spliced.length - 1];
    }

    private void fillBundleFromNotification(String id, Bundle bundle) {
        bundle.putString(App.CONSTANT.LINK.toString(), "fcinternews.it-" + id);
        bundle.putBoolean(App.CONSTANT.IS_FROM_NOTIFICATION.toString(), true);
        bundle.putBoolean(App.CONSTANT.SHOW_INTERSTITIAL.toString(), false);
    }
    
    private void resetTabList() {
        listCategories.clear();

        // --------------------- GET SAVED TABS LIST -------------------------
        SharedPreferences sharedPrefDefault = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        String savedTabsString = sharedPrefDefault.getString(getResources().getString(R.string.pref_tab_menu), "0");

        // --------------------- GET SAVED TABS LIST OR NEW LIST -------------------------
        if (!savedTabsString.equals("0")) {
            SettingsTabMenuUtility.parseTabsFromString(savedTabsString, hashMapCategories, listCategories);
        } else {
            listCategories = (ArrayList<Category>) Categories.LIST_CATEGORIES.clone();
            listCategories.remove(0);
            listCategories.remove(0);
        }
    }
    
    private void setTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.removeAllTabs();

        newsListFragment.setTabLayout(tabLayout);

        // set first tab ( all news )
        TabLayout.Tab firstTab = tabLayout.newTab();
        firstTab.setTag(2);  // all news id is 2
        firstTab.setText(R.string.tab_all_news);
        tabLayout.addTab(firstTab, true);

        // set others tab
        for (Category category : listCategories) {
            if (category.isShowOnTabMenu()) {
                TabLayout.Tab currentTab = tabLayout.newTab();
                currentTab.setTag(category.getId());
                currentTab.setText(category.getTitle());
                tabLayout.addTab(currentTab, false);
            }
        }

        tabSelectedListener = new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int intTab = (int) tab.getTag();
                refillRecyclerView(hashMapCategories.get(intTab));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int intTab = (int) tab.getTag();
                refillRecyclerView(hashMapCategories.get(intTab));
            }
        };

        // on the first launch of the app set immediately the select listener
        if (App.getSelectedCategory() == null)
            tabLayout.addOnTabSelectedListener(tabSelectedListener);
            // on the other launches of the app set the select listener after
            // because I select programmatically the tab on tablayout and I don't want to call onTabSelected in this case
        else
            SettingsTabMenuUtility.selectItemOnTabMenu(App.getSelectedCategory(), self, tabSelectedListener);

    }
    
    // ---------------------------- ADS -----------------------------------

    private void createElements() {
        if (toInitialize) {
            createAdMAST();
            toInitialize = false;
        }
    }

    private void createAdMAST() {
        adInlineAdView_320_50 = findViewById(R.id.adInlineAdView_320_50);
        SASAdView.setBaseUrl(getString(R.string.ad_server_base_url));
        adInlineAdView_320_50.loadAd(235558, String.valueOf(935188), 64093, true, "", new SASAdView.AdResponseHandler() {
            @Override
            public void adLoadingCompleted(SASAdElement sasAdElement) { }

            @Override
            public void adLoadingFailed(Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadAdMob320_50();
                    }
                });
            }
        });
    }

    private void loadAdMob320_50() {
        adInlineAdView_320_50.setVisibility(View.GONE);
        AdView admob = findViewById(R.id.admobAdViewHomepage);
        admob.setVisibility(AdView.VISIBLE);
        AdRequest adRequest;
        adRequest = new AdRequest.Builder()
                //.addTestDevice("91CCB7EA1719B72B07721C85895732EB")
                .build();
        admob.loadAd(adRequest);
    }

    private void closeDrawer() {
        // close the drawler
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    void refillRecyclerView(Category category) {
        App.setSelectedCategory(category);

        Bundle bundle = new Bundle();
        bundle.putString(App.CONSTANT.TITLE.toString(), category.getTitle());
        bundle.putString(App.CONSTANT.LINK.toString(), category.getLink());

        newsListFragment.isFromMenu = true;
        newsListFragment.updateDisplayFromActivity();
        newsListFragment.setLinkAndRefresh(bundle);
    }

    @Override
    public void onOSPermissionChanged(OSPermissionStateChanges stateChanges) {
        if (stateChanges.getFrom().getEnabled() &&
                !stateChanges.getTo().getEnabled()) {
            new AlertDialog.Builder(this)
                    .setMessage("Notifications Disabled!")
                    .show();
        }

        Log.i("Debug", "onOSPermissionChanged: " + stateChanges);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}

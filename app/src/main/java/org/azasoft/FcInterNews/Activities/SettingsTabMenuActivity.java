package org.azasoft.FcInterNews.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.Fragments.NewsDetailFragment;
import org.azasoft.FcInterNews.Models.Categories;
import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.Models.SettingsTabMenuAdapter;
import org.azasoft.FcInterNews.R;

import java.util.ArrayList;

public class SettingsTabMenuActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    //Categories categories = new Categories();
    //final ArrayList<Category> listCategories = Categories.LIST_CATEGORIES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_tab_menu);

        // ------------ SET TOOLBAR -------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_customize_tab_menu);
        setSupportActionBar(toolbar);

        // ------------ SET UP BUTTON -------------------------------------
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        /*
        // ------------ SET RECYCLER VIEW -------------------------------------
        recyclerView = (RecyclerView) findViewById(R.id.rv_settings_tab_menu);
        recyclerView.setHasFixedSize(true);

        // --------------- set layout menager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        // --------------- remove first 2 elements from listCategories
        listCategories.remove(0);  // remove store online
        listCategories.remove(0);  // remove all news ( home page )

        // --------------- set adapter
        SettingsTabMenuAdapter adapter = new SettingsTabMenuAdapter(getApplicationContext(), listCategories);
        recyclerView.setAdapter(adapter);
        */
    }
}

package org.azasoft.FcInterNews.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import org.azasoft.FcInterNews.R;

public class SettingsPushActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_push);

        // ------------ SET TOOLBAR -------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_customize_push);
        setSupportActionBar(toolbar);

        // ------------ SET UP BUTTON -------------------------------------
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
}

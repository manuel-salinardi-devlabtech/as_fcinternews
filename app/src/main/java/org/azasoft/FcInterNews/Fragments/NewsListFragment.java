package org.azasoft.FcInterNews.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.azasoft.FcInterNews.Activities.NewsDetailActivity;
import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.RSS.RSSFeed;
import org.azasoft.FcInterNews.RSS.RSSItem;
import org.azasoft.FcInterNews.RSS.Parser;
import org.azasoft.FcInterNews.RecyclerViewUtility.EndlessRecyclerViewScrollListener;
import org.azasoft.FcInterNews.Models.NewsAdapter;
import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsListFragment extends Fragment implements View.OnClickListener {

    Activity homePageActivity;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayout;
    NewsAdapter myAdapter;
    TabLayout tabLayout;
    View view;

    private String SHAREDPREF = "SharedPref";
    private int ArticoliLetti = 0;
    private Date LastRecordedDate = Calendar.getInstance().getTime();
    private Map<String, Object> targetParams = new HashMap<String, Object>();

    private int start = 0;
    private int limit = 20;
    private boolean toInitialize = true;

    public void setHomePageActivity(Activity activity) {
        this.homePageActivity = activity;
    }
    public boolean isFromRefresh() {
        return isFromRefresh;
    }

    public void setFromRefresh(boolean fromRefresh) {
        isFromRefresh = fromRefresh;
    }

    public void setTabLayout(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
    }

    public boolean isFromMenu = false;

    private boolean isFromRefresh = false;

    public String getTitle() {
        return title;
    }
    public String linkRss = "";
    public final String tag = "RSSReader";
    private RSSFeed feed = null;
    private List<RSSItem> items = null;
    private ProgressDialog dialog;
    GetFeedTask _feedTask;
    GetImagesTask _imagesTask;
    //ElementiAdapter myAdapter;
    String title = "";

    Object lock = new Object();

    public NewsListFragment() {
        // Required empty public constructor
    }

    public void scrollToFirstItem() {
        // move recyclerview to first position
       // LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
        //llm.scrollToPositionWithOffset(0 , items.size());
        recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        view = inflater.inflate(R.layout.fragment_news_list, container, false);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UpdateDisplay(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setLinkAndRefresh(Bundle args) {
        if (args != null) {
            linkRss = args.getString(App.CONSTANT.LINK.toString());
            title = args.getString(App.CONSTANT.TITLE.toString());
        }

        if (_feedTask == null || _feedTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            //getActivity().setTitle(title);
            dialog = ProgressDialog.show(getActivity(), "", "Caricamento...", true);
            _feedTask = new GetFeedTask();
            _feedTask.execute(getActivity());
        }
    }

    public void updateDisplayFromActivity() {
        start = 0;
        isFromRefresh = true;
        scrollToFirstItem();
        UpdateDisplay(null);
    }

    public void UpdateDisplay(View v) {
        isFromRefresh = false;
        if (getActivity() != null) {
            if ((items == null || items.size() == 0) && feed != null)
                items = feed.getAllItems();

            if (items == null)
                items = new ArrayList<RSSItem>();

            if (v != null)
                recyclerView = v.findViewById(R.id.recycler_view_news_list);
            else
                recyclerView = getActivity().findViewById(R.id.recycler_view_news_list);

            recyclerView.setAdapter(null);
            myAdapter = new NewsAdapter(
                    items,
                    getActivity(),
                    this
            );

            recyclerView.setAdapter(myAdapter);
            linearLayout = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayout);
            if (recyclerView.getItemDecorationCount() == 0) {
                RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
                recyclerView.addItemDecoration(itemDecoration);
            }
            EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayout) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    loadData(page, totalItemsCount, view);
                }
            };
            recyclerView.addOnScrollListener(scrollListener);
        }
    }
    
    private RSSFeed getFeed(String urlToRssFeed) {
        try {
            return Parser.ParseLista(urlToRssFeed, "0", start, limit);
        } catch (Exception ee) {
            // if we have a problem, simply return null
            return null;
        }
    }

    public void loadData(int page, int totalItemsCount, RecyclerView view) {
        start += limit;
        _feedTask = new GetFeedTask();
        _feedTask.execute(getActivity());
        //TabLayoutUtility.disableClick(tabLayout);
    }

    protected class GetFeedTask extends AsyncTask<Context, Integer, String> {
        @Override
        protected String doInBackground(Context... params) {
            // popolo il feed
            feed = getFeed(linkRss);
            return "finito";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("finito")) {

                if (isFromRefresh) {
                    UpdateDisplay(null);
                } else {
                    int curSize = items.size();
                    if (isFromMenu) {
                        items.clear();
                        isFromMenu = false;
                    }
                    List<RSSItem> allItems = feed.getAllItems();

                    if (items.size() > 1 && allItems.size() > 0) {
                        String feedFirstElmentLink = allItems.get(0).getLink();
                        String itemsLastElmentLink = items.get(items.size()-1).getLink();

                        // if the last element in the recyclerview is the same of the first just load from the net
                        if (feedFirstElmentLink.equals(itemsLastElmentLink)) {
                            allItems.remove(0);
                        }
                    }

                    items.addAll(feed.getAllItems());
                   // myAdapter.notifyItemRangeChanged(curSize, items.size());
                    myAdapter.notifyDataSetChanged();
                }

                // show or hide no data message
                if (items.isEmpty()) {
                    homePageActivity.findViewById(R.id.no_data_available).setVisibility(View.VISIBLE);
                    homePageActivity.findViewById(R.id.admobAdViewHomepage).setVisibility(View.GONE);
                    homePageActivity.findViewById(R.id.adInlineAdView_320_50).setVisibility(View.GONE);
                    homePageActivity.findViewById(R.id.admobAdViewHomepage).setVisibility(View.GONE);
                    homePageActivity.findViewById(R.id.fab).setVisibility(View.GONE);
                } else {
                    homePageActivity.findViewById(R.id.no_data_available).setVisibility(View.GONE);
                    homePageActivity.findViewById(R.id.admobAdViewHomepage).setVisibility(View.VISIBLE);
                    homePageActivity.findViewById(R.id.adInlineAdView_320_50).setVisibility(View.VISIBLE);
                    homePageActivity.findViewById(R.id.admobAdViewHomepage).setVisibility(View.VISIBLE);
                    homePageActivity.findViewById(R.id.fab).setVisibility(View.VISIBLE);
                }

                SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
                mSwipeRefreshLayout.setRefreshing(false);

                if (dialog != null)
                    dialog.dismiss();

                if (_imagesTask == null
                        || _imagesTask.getStatus().equals(
                        AsyncTask.Status.FINISHED)) {
                    if (getActivity() != null
                            && getActivity().getApplicationContext() != null) {
                        _imagesTask = new GetImagesTask();
                        _imagesTask.execute(getActivity()
                                .getApplicationContext());
                    }
                }
            }
        }
    }

    protected class GetImagesTask extends AsyncTask<Context, Integer, String> {
        @SuppressWarnings("unchecked")
        @Override
        protected String doInBackground(Context... params) {

            if (feed != null && items == null) {
                items = feed.getAllItems();
            }
            if (items != null) {
                for (Integer i = 0; i < items.size(); i++) {
                    if (items.get(i).getIsBigNews()) {
                        items.get(i).setImmagine( Utility.GetImageBitmap(params[0], items.get(i).getEnclosureBig()));
                    } else {
                        items.get(i).setImmagine( Utility.GetImageBitmap(params[0], items.get(i).getEnclosure()));
                    }
                    publishProgress(i);
                }
            }
            return "finito";
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            synchronized(lock) {
                myAdapter.notifyItemChanged(progress[0]);
            }
        }

        @Override
        protected void onPostExecute(String result) {

        }

    }

    public void refresh(View v) {
        if (_feedTask == null
                || _feedTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            recyclerView.setAdapter(null);
            items.clear();
            isFromRefresh = true;
            start = 0;
            _feedTask = new GetFeedTask();
            _feedTask.execute(getActivity());
        }
    }

    Bundle b = new Bundle();

    @SuppressWarnings("rawtypes")
    public void onClick(View v) {
        int position = (int)v.getTag();

        if (position >= 0) {

            RSSItem currentItem = myAdapter.getItem(position);

            Log.i(tag, "item clicked! [" + myAdapter.getItem(position).getTitle()
                    + "]");

            b.putString("titolo", currentItem.getTitle());
            b.putString("categoria", currentItem.getCategory());
            // b.putString("description", feed.getItem(position).getDescription());

            b.putString(App.CONSTANT.LINK.toString(), currentItem.getLink());
            b.putString("data", currentItem.getPubDate());
            b.putBoolean("immagineDisponibile", currentItem.getEnclosure() != null);

            // cause crash on first element recyclerView
            //b.putParcelable("immagine", currentItem.getImmagine());
            b.putString("mainTitolo", title);
            b.putString("mainLink", linkRss);

            ArrayList<String> urlList = new ArrayList<String>();
            for (int i = 0; i < myAdapter.getItemCount(); i++) {
                urlList.add(myAdapter.getItem(i).getLink());
            }
            b.putStringArrayList("urlList", urlList);
            b.putInt("position", position);

            SharedPreferences settings = getActivity().getSharedPreferences(
                    SHAREDPREF, 0);
            SharedPreferences.Editor editor = settings.edit();

            if (!settings.contains("canShowOgury")) {
                editor.putBoolean("canShowOgury", true);
                editor.commit();
            }

            if (Calendar.getInstance().getTime().getTime()
                    - LastRecordedDate.getTime() > 86400000) {
                // l'ultima data è di ieri la setto a oggi e inizio a contare.
                LastRecordedDate = Calendar.getInstance().getTime();
                ArticoliLetti = 1;
                editor.putBoolean("canShowOgury", true);
                editor.commit();
            } else {
                ArticoliLetti += 1;
                editor.putBoolean("showOguryInterstitial", false);
            }

            if (ArticoliLetti == 1) {
                b.putBoolean("showOguryInterstitial", true);
            } else {
                b.putBoolean("showOguryInterstitial", false);
            }

            // ArticoliLetti = 0;
            if (ArticoliLetti > 5) {
                ArticoliLetti = 1;

                b.putBoolean("showInterstitial", true);
            } else {
                b.putBoolean("showInterstitial", false);
            }

            b.putInt("ArticoliLetti", ArticoliLetti);


            Log.i(tag, "bundle " + b.toString());

            // TODO-manuel tablet mode
            //View fragmentContainer = view.findViewById(R.id.detail_fragment);
            // if we are in tablet mode
            /*
            if (fragmentContainer != null) {
                NewsDetailFragment details = new NewsDetailFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                //details.setNewsId(index);
                ft.replace(R.id.detail_fragment, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
            } else { */
                Intent itemintent = new Intent(getActivity(), NewsDetailActivity.class);
                itemintent.putExtra("android.intent.extra.INTENT", b);
                startActivity(itemintent);
            //}
        }
    }
    @SuppressLint("SimpleDateFormat")
    @Override
    public void onStop() {
        super.onStop();
        dialog = null;

        SharedPreferences settings = getActivity().getSharedPreferences(
                SHAREDPREF, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putInt("ArticoliLetti", ArticoliLetti);
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        editor.putString("LastRecordedDate",
                df.format(Calendar.getInstance().getTime()));

        // Commit the edits!
        editor.commit();
    }

    @Override
    public void onAttach(Activity activity) {
        setRetainInstance(true);

        SharedPreferences settings = activity.getSharedPreferences(SHAREDPREF,
                0);
        ArticoliLetti = settings.getInt("ArticoliLetti", 0);
        String sLastRecordedDate = settings.getString("LastRecordedDate", "");

        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        try {
            LastRecordedDate = df.parse(sLastRecordedDate);
        } catch (ParseException e) {

        }
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        _feedTask = null;
    }

    @Override
    public void onDestroyView() {
        recyclerView = null;
        start = 0;
        super.onDestroyView();
    }
}

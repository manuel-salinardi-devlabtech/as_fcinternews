package org.azasoft.FcInterNews.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.smartadserver.android.library.SASBannerView;
import com.smartadserver.android.library.SASInterstitialView;
import com.smartadserver.android.library.model.SASAdElement;
import com.smartadserver.android.library.ui.SASAdView;

import org.azasoft.FcInterNews.Activities.NewsDetailActivity;
import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.Models.Categories;
import org.azasoft.FcInterNews.Models.Detail;
import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.RSS.Parser;
import org.azasoft.FcInterNews.Utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailFragment extends Fragment implements YouTubePlayer.OnInitializedListener {

    View currentView;
    FragmentActivity mContext;
    NewsDetailFragment _this;

    private String MainTitolo;
    private String MainLink;
    private Bitmap image = null;
    private String link;

    ShareActionProvider shareActionProvider;



    private long newsId;

    private ProgressDialog dialog;
    private Detail dett;
    private ScrollView scrollView;

    // YOU TUBE
    YouTubePlayerSupportFragment youTubePlayerFragment;
    boolean youtubeViewInitialized = false;
    private YouTubePlayer.OnInitializedListener fragment;
    private static final String YouTubeAPIKey = "AIzaSyC6h5zKySgAsrQLj91xr1WBpRSXKpYwZzA";
    private YouTubePlayer YPlayer;
    String videoId = "";

    private int position, articoliLetti;
    public SASBannerView adInlineAdView_300_250;
    private SASBannerView adInlineAdView_320_50;
    private InterstitialAd admobInterstitial;
    private SASInterstitialView mInterstitialView;
    private boolean showInterstitial = false;



    private String SHAREDPREF = "SharedPref";

    GetImagesTask _imagesTask;


    private ArrayList<String> urlList;
    private Object lock = new Object();



    public void setNewsId(long id) {
        this.newsId = id;
    }

    public NewsDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        MenuItem menuItem = menu.findItem(R.id.action_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setShareActionIntent(String soggetto, String contenuto) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, soggetto);
        intent.putExtra(Intent.EXTRA_TEXT, contenuto);
        shareActionProvider.setShareIntent(intent);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.currentView = view;
        mContext = getActivity();
        _this = this;

        scrollView = view.findViewById(R.id.scroll_detail);

        // ----------------- INITIALIZE SMARTADSSERVER ----------------------
        adInlineAdView_300_250 = view.findViewById(R.id.adInlineAdView_300_250);
        adInlineAdView_320_50 = view.findViewById(R.id.adInlineAdView_320_50);

        mInterstitialView = new SASInterstitialView(mContext);

        SASAdView.setBaseUrl(mContext.getString(R.string.ad_server_base_url));
        SASAdView.setBaseUrl(mContext.getString(R.string.ad_server_base_url));
        SASAdView.setBaseUrl(mContext.getString(R.string.ad_server_base_url));

        //fragment = this;

            Intent startingIntent = mContext.getIntent();

            if (startingIntent != null) {
                Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
                if (b == null) {
                    TextView titolo = (TextView) view.findViewById(R.id.detail_title);
                    titolo.setText("Errore");
                } else {
                    articoliLetti = b.getInt("ArticoliLetti");
                    MainTitolo = b.getString("mainTitolo");

                    urlList = new ArrayList<>();
                    if (b.containsKey("urlList"))
                        urlList = b.getStringArrayList("urlList");

                    position = 0;
                    if (b.containsKey("position"))
                        position = b.getInt("position");

                    showInterstitial = b.getBoolean("showInterstitial");

                    image = null;
                    link = b.getString(App.CONSTANT.LINK.toString());
                    TextView titolo = (TextView) view.findViewById(R.id.detail_title);
                    titolo.setText(b.getString("titolo"));

                    dialog = ProgressDialog.show(getActivity(), "", "Caricamento...", true);
                    if (b.getBoolean("immagineDisponibile")) {
                        image = b.getParcelable("immagine");
                    }

                    checkInterstitial();
                    GetDetailsTask _detailsTask = new GetDetailsTask();
                    _detailsTask.execute(getActivity());
                }
            }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    protected class GetDetailsTask extends AsyncTask<Context, Integer, String> {
        @Override
        protected String doInBackground(Context... params) {
            dett = Parser.Parse(link);
            return "finito";
        }

        @Override
        protected void onPostExecute(String result) {
            // update dell'activity
            if (dett.getText().equals("")) {
                Toast noConnection = Toast.makeText(getActivity().getBaseContext(),
                        "Impossibile caricare.\nVerificare la connessione",
                        Toast.LENGTH_LONG);
                noConnection.show();
                getActivity().finish();
            } else {

                WebView webView = currentView.findViewById(R.id.dettTesto);

                String data;
                try {
                    data = URLEncoder.encode(dett.getText(), "UTF-8").replaceAll("\\+", " ");
                } catch (UnsupportedEncodingException e) {
                    throw new AssertionError("UTF-8 not supported");
                }

                WebSettings settings = webView.getSettings();
                settings.setDefaultTextEncodingName("utf-8");
                settings.setTextZoom(110);

                webView.loadData(data, "text/html; charset=utf-8", "UTF-8");
                webView.setBackgroundColor(NewsDetailActivity.BACKGROUND_COLOR_APPTHEME);

                if (_imagesTask == null
                        || _imagesTask.getStatus().equals(
                        AsyncTask.Status.FINISHED)) {
                    _imagesTask = new GetImagesTask();
                    _imagesTask.execute(getActivity().getApplicationContext());
                }

                TextView detailLabel = currentView.findViewById(R.id.detail_label);
                String labelText = dett.getCategory();
                detailLabel.setText(labelText);

                // change color of label
                String colorLabel = Categories.getCategoryColor(labelText);
                detailLabel.getBackground().setColorFilter(Color.parseColor(colorLabel), PorterDuff.Mode.SRC_OVER);

                TextView detailAuthor = (TextView) currentView.findViewById(R.id.detail_author);
                detailAuthor.setText(dett.getAuthor() + "  ");
                //TextView letture = (TextView) findViewById(R.id.letture);
                //letture.setText(dett.getLetture());
                TextView titolo = (TextView) currentView.findViewById(R.id.detail_title);

                TextView detailDate = (TextView) currentView.findViewById(R.id.detail_date);
                String date = dett.getDate();
                detailDate.setText(date.substring(0, date.length()-3));

                titolo.setText(Html.fromHtml(dett.getTitle()).toString());

                LinearLayout lin = currentView.findViewById(R.id.videoContainer);
                lin.removeAllViews();

                //TextView titolo = (TextView) currentView.findViewById(R.id.detail_title);
                setShareActionIntent(Html.fromHtml(dett.getTitle()).toString(), Detail.getShareUrl());

                if (dett.getLinkYoutube() != null && dett.getLinkYoutube().size() > 0) {

                    for (int i = 0; i < dett.getLinkYoutube().size(); i++) {

                        String link = dett.getLinkYoutube().get(i).getLink();

                        String regExp = "/.*(?:youtu.be\\/|v\\/|u/\\w/|embed\\/|watch\\?.*&?v=|video_id=)";
                        Pattern compiledPattern = Pattern.compile(regExp);
                        Matcher matcher = compiledPattern.matcher(link);

                        if (matcher.find()){
                            int start = matcher.end();
                            videoId = link.substring(start, start + 11);

                            // ----------------- INITIALIZE YOUTUBE ----------------------
                            youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
                            youTubePlayerFragment.initialize(YouTubeAPIKey, _this);

                            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                            fragmentTransaction.add(R.id.youtube_fragment, youTubePlayerFragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        } else {
                            TextView tmp = new TextView(mContext.getBaseContext());
                            tmp.setTypeface(Typeface.create(Typeface.DEFAULT_BOLD,
                                    Typeface.ITALIC));
                            SpannableString contentUnderline = new SpannableString("Video #" + String.valueOf(i+1) + ": " +
                                    dett.getLinkYoutube().get(i).getTesto());
                            contentUnderline.setSpan(new UnderlineSpan(), 0,

                                    contentUnderline.length(), 0);
                            tmp.setText(contentUnderline);
                            tmp.setTextColor(0xff000000);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(60, 4, 10, 60); // left, top, right,
                            // bottom
                            tmp.setLayoutParams(params);
                            final String linkVideo = link;
                            tmp.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    startActivity(new Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse(linkVideo)));
                                }
                            });
                            lin.addView(tmp);
                        }
                    }

                }
            }

            createElements();

            if (dialog != null)
                dialog.dismiss();

        }
    }

    private void createElements() {



        //Banner 320x250
        if (adInlineAdView_300_250 != null) {
            adInlineAdView_300_250.loadAd(235558, "935188", 64094, true, "", new SASAdView.AdResponseHandler() {


                @Override
                public void adLoadingCompleted(SASAdElement sasAdElement) {
                    Log.d("Ads", "Load success AdInline 320x250");
                }

                @Override
                public void adLoadingFailed(Exception e) {
                    Log.d("Ads", "SmartAD not loaded, switch to AdMob");

                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adInlineAdView_300_250.setVisibility(View.GONE);
                            AdView admob = (AdView) currentView.findViewById(R.id.admobAdViewDetailFooter);
                            admob.setVisibility(View.VISIBLE);
                            AdRequest adRequest;
                            adRequest = new AdRequest.Builder().build();
                            admob.loadAd(adRequest);
                        }
                    });

                }
            });

        }


        //Banner 320x50
        if (adInlineAdView_320_50 != null) {

            adInlineAdView_320_50.loadAd(235558, "935188", 64093, true, "", new SASAdView.AdResponseHandler() {
                @Override
                public void adLoadingCompleted(SASAdElement sasAdElement) {
                    Log.d("Ads", "Load success AdInline 300x50");
                }

                @Override
                public void adLoadingFailed(Exception e) {
                    Log.d("Ads", "Integrating Inline Ad not loaded, switch to AdMob");

                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adInlineAdView_320_50.setVisibility(View.GONE);
                            AdView admob = (AdView) currentView.findViewById(R.id.admobAdViewDetail);
                            admob.setVisibility(View.VISIBLE);
                            AdRequest adRequest;
                            adRequest = new AdRequest.Builder().build();
                            admob.loadAd(adRequest);
                        }
                    });
                }
            });
        }
    }

    protected class GetImagesTask extends AsyncTask<Context, Integer, String> {

        @Override
        protected String doInBackground(Context... params) {
            image = Utility.GetImageBitmap(getActivity(), dett.getImage());
            return "finito";
        }

        @Override
        protected void onPostExecute(String result) {
            synchronized(lock) {
                if (image != null) {
                    ImageView immagine = (ImageView) currentView.findViewById(R.id.detail_image);
                    immagine.setImageBitmap(image);
                }
            }
        }

    }

    public boolean prevNews () {
        scrollView.scrollTo(0,0);

        TextView titolo = (TextView) currentView.findViewById(R.id.detail_title);
        setShareActionIntent(titolo.getText().toString(), Detail.getShareUrl());

        if (youTubePlayerFragment != null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.remove(youTubePlayerFragment);
            fragmentTransaction.commit();
        }

        GetDetailsTask _detailsTask = new GetDetailsTask();

        position = position - 1;
        if (position < 0) {
            position = 0;
            return false;
        }

        String prevurl = urlList.get(position);
        if (prevurl == null || prevurl.isEmpty()) {
            this.position = position + 1;
            if (position >= urlList.size()) {
                position = urlList.size() - 1;
                return false;
            }
            prevurl = urlList.get(position);
        }

        setArticoliLetti();

        checkInterstitial();

        link = prevurl.replaceFirst("www", "m");
        dialog = ProgressDialog.show(getActivity(), "", "Caricamento...", true);
        _detailsTask.execute(getActivity());
        return true;
    }

    public boolean nextNews () {
        scrollView.scrollTo(0,0);

        if (youTubePlayerFragment != null) {
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.remove(youTubePlayerFragment);
            fragmentTransaction.commit();
        }
        GetDetailsTask _detailsTask = new GetDetailsTask();

        this.position = position + 1;
        if (position >= urlList.size()) {
            position = urlList.size() - 1;
            return false;
        }

        String nexturl = urlList.get(position);
        if (nexturl == null || nexturl.isEmpty()) {
            this.position = position + 1;
            if (position >= urlList.size()) {
                position = urlList.size() - 1;
                return false;
            }
            nexturl = urlList.get(position);
        }
        setArticoliLetti();

        checkInterstitial();

        link = nexturl.replaceFirst("www", "m");
        dialog = ProgressDialog.show(getActivity(), "", "Caricamento...", true);
        _detailsTask.execute(getActivity());
        return true;
    }

    private void setArticoliLetti() {
        articoliLetti = articoliLetti + 1;

        SharedPreferences settings = getActivity().getSharedPreferences(
                SHAREDPREF, 0);
        SharedPreferences.Editor editor = settings.edit();

        if (articoliLetti > 5) {
            showInterstitial = true;
            articoliLetti = 1;
        }

        editor.putInt("ArticoliLetti", articoliLetti);
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        editor.putString("LastRecordedDate",
                df.format(Calendar.getInstance().getTime()));

        // Commit the edits!
        editor.commit();
    }

    private void checkInterstitial() {

        if (showInterstitial) {
            showInterstitial = false;

            mInterstitialView.loadAd(235558, "935188", 64095, true, "", new SASAdView.AdResponseHandler() {
                @Override
                public void adLoadingCompleted(SASAdElement sasAdElement) {
                    Log.d("Interstitial", "Mostro interstitial AdOverlay");
                }

                @Override
                public void adLoadingFailed(Exception e) {
                    showAdMobInterstitial();
                }
            });
        }

    }

    private void showAdMobInterstitial() {
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("Interstitial", "Leonardo non fruisce. Mostro AdMob");
                admobInterstitial = new InterstitialAd(mContext);
                admobInterstitial.setAdUnitId(mContext.getResources().getString(R.string.admob_interstitial_id));

                AdListener listener = new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        admobInterstitial.show();
                    }
                };

                admobInterstitial.setAdListener(listener);

                AdRequest adRequest;
                adRequest = new AdRequest.Builder().build();

                admobInterstitial.loadAd(adRequest);
            }
        });
    }

    // -------------- YOU TUBE ------------------

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (adInlineAdView_300_250 != null) {
            adInlineAdView_300_250.onDestroy();
        }
        if (adInlineAdView_320_50 != null) {
            adInlineAdView_320_50.onDestroy();
        }

        if (mInterstitialView != null)
            mInterstitialView.onDestroy();
    }

    // ----------------- YOU TUBE EVENTS ------------------------

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            YPlayer = youTubePlayer;
            //YPlayer.loadVideo(videoId);
            YPlayer.cueVideo(videoId);
            YPlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }



}

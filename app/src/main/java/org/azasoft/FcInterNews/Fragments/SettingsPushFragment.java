package org.azasoft.FcInterNews.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.view.View;

import com.onesignal.OneSignal;

import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.Utility;

public class SettingsPushFragment extends PreferenceFragmentCompat {

    private SwitchPreferenceCompat switch_notification = null;
    private Activity activity;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = getActivity();

        switch_notification = (SwitchPreferenceCompat) findPreference(getString(R.string.pref_key_notification));
        SharedPreferences sharedPref = this.activity.getSharedPreferences(this.getString(R.string.fcinternews_preference_file_key), Context.MODE_PRIVATE);
        boolean setting_notification = sharedPref.getBoolean(this.getString(R.string.pref_key_notification), true);

        if (setting_notification) {
            switch_notification.setChecked(true);
            switch_notification.setSummary(getString(R.string.pref_summary_notification_on));
        }
        else {
            switch_notification.setChecked(false);
            switch_notification.setSummary(getString(R.string.pref_summary_notification_off));
        }

        switch_notification.setOnPreferenceChangeListener(new android.support.v7.preference.Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(android.support.v7.preference.Preference preference, Object newValue) {
                Context context = activity.getApplicationContext();
                boolean value = (boolean) newValue;
                if (value) {
                    switch_notification.setSummary(getString(R.string.pref_summary_notification_on));
                    Utility.PutPreference(context, R.string.pref_key_notification, true, R.string.fcinternews_preference_file_key);
                    OneSignal.setSubscription(true);
                } else {
                    switch_notification.setSummary(getString(R.string.pref_summary_notification_off));
                    Utility.PutPreference(context, R.string.pref_key_notification, false, R.string.fcinternews_preference_file_key);
                    Utility.PutPreference(context, R.string.saved_counter_notification, context.getResources().getInteger(R.integer.notification_counter_default), R.string.fcinternews_preference_file_key);
                    OneSignal.setSubscription(false);
                }
                return true;
            }
        });


    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the Preferences from the XML file
        addPreferencesFromResource(R.xml.preferences);
    }
}

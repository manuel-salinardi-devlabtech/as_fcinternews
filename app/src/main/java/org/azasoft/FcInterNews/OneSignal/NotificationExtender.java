package org.azasoft.FcInterNews.OneSignal;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.azasoft.FcInterNews.Activities.HomePageActivity;
import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.Utility;

/**
 * Created by gammella on 28/11/17.
 */

public class NotificationExtender extends NotificationExtenderService {
    private Context mContext;
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read properties from result.
        boolean returnValue = false;
        mContext = App.getContext();
        SharedPreferences sharedPrefNotif = mContext.getSharedPreferences(mContext.getString(R.string.fcinternews_preference_file_key), Context.MODE_PRIVATE);
        boolean setting_notification = sharedPrefNotif.getBoolean(mContext.getString(R.string.pref_key_notification), true);

        //se la ricezione delle notifiche sono disabilitate ad ogni ricezione decremento il contatore
        if (!setting_notification) {
            returnValue = true;
            int counter_notification = Utility.GetPreference(mContext, R.string.saved_counter_notification, mContext.getResources().getInteger(R.integer.notification_counter_default), R.string.fcinternews_preference_file_key);
            if (counter_notification > 0) {
                Utility.PutPreference(mContext, R.string.saved_counter_notification, counter_notification - 1, R.string.fcinternews_preference_file_key);
            }
        } /*else {
            //altrimenti la ricezione delle notifiche è attivo quindi invio la notifica
            String title = receivedResult.payload.title;
            String message = receivedResult.payload.body;
            String url = receivedResult.payload.launchURL;

            sendNotification(title, message, url);
        }*/
        // Return true to stop the notification from displaying.
        return returnValue;
    }


    private void sendNotification(String title, String message, String url) {
        mNotificationManager = (NotificationManager)
                mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("title", title);
        bundle.putString("message", message);
        bundle.putBoolean("fromNotification", true);

        Intent i = new Intent(mContext, HomePageActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra("FcInterNewsNotification", bundle);

        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = {500,500,500};

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext)
                        .setSmallIcon(R.drawable.ic_stat_logointernews_notifica)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(title))
                        .setSound(alarmSound)
                        .setLights(Color.BLUE, 500, 500)
                        .setVibrate(pattern)
                        .setContentText(message);

        mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
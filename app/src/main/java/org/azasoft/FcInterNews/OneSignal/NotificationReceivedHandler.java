package org.azasoft.FcInterNews.OneSignal;

import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by gammella on 28/11/17.
 */

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String customKey;
        Log.i("FCINT_NReceived", "----");
        if (data != null) {
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("FCINT_NReceived", "customkey set with value: " + customKey);
        }
    }
}
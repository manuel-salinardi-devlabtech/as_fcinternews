package org.azasoft.FcInterNews.OneSignal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.azasoft.FcInterNews.Activities.HomePageActivity;
import org.azasoft.FcInterNews.App;
import org.json.JSONObject;

/**
 * Created by gammella on 28/11/17.
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String customKey;
        Context context = App.getContext();

        if (data != null) {
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("FCINT_NOpened", "customkey set with value: " + customKey);
        }

        App.linkFromPushNotification = result.notification.payload.launchURL;
        App.isFromPushNotification = true;

        Bundle bundle = new Bundle();
        bundle.putString("url", result.notification.payload.launchURL);
        bundle.putString("title", result.notification.payload.title);
        bundle.putString("message", result.notification.payload.body);
        bundle.putBoolean("fromNotification", true);

        Intent intent = new Intent(context, HomePageActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("FcInterNewsNotification", bundle);
        context.startActivity(intent);
    }
}
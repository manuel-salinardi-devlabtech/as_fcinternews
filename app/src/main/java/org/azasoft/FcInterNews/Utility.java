package org.azasoft.FcInterNews;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;

import com.google.common.io.Resources;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utility {
	
	private static Bitmap logo;
	
	public static Bitmap GetImageBitmap(Context cont, String url) {
		Bitmap bm = null; 
		if (url==null)
		{
			if (logo == null) {
				logo = BitmapFactory.decodeResource(cont.getResources(),R.drawable.placeholder_image);

			}
			return logo;
		}
		else
		{
			try { 
	            URL aURL = new URL(url); 
	            URLConnection conn = aURL.openConnection(); 
	            conn.connect(); 
	            InputStream is = conn.getInputStream(); 
	            BufferedInputStream bis = new BufferedInputStream(is); 
	            bm = BitmapFactory.decodeStream(bis); 
	            bis.close(); 
	            is.close(); 
	       } catch (IOException e) { 
	           Log.e("Gio", url, e); 
	       } 
		}
       return bm; 
    }

    public static void PutPreference(Context context, int key, int newValue, int prefKey) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(prefKey), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(context.getString(key), newValue);
        editor.commit();
    }

    public static void PutPreference(Context context, int key, boolean newValue, int prefKey) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(prefKey), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(context.getString(key), newValue);
        editor.commit();
    }

    public static int GetPreference(Context context, int key, int defaultValue, int prefKey) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(prefKey), Context.MODE_PRIVATE);
        return sharedPref.getInt(context.getString(key), defaultValue);
    }

	public static String getHash(StringBuilder params, String uuid) {
		String salt = "nmv6c2xz57";
		String prepareHashString = String.format("%s?%s%s%s", "https://fcin-android-d9f3.articles-pub.v1.tccapis.com/", params.toString(), salt, uuid);

		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] hashByte = new byte[0];

		try {
			hashByte = digest.digest(prepareHashString.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		StringBuilder hexString = new StringBuilder();
		for (byte aMessageDigest : hashByte) {
			String h = Integer.toHexString(0xFF & aMessageDigest);
			while (h.length() < 2)
				h = "0" + h;
			hexString.append(h);
		}

		return hexString.toString();
	}

	public static void EnableSSL() {
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}

					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}
	}
}



package org.azasoft.FcInterNews.SettingsTabMenu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.azasoft.FcInterNews.Models.Categories;
import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.R;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private ArrayList<Category> listCategories;


    private final OnStartDragListener mDragStartListener;

    RecyclerListAdapter(ArrayList<Category> listCategories, OnStartDragListener dragStartListener) {
        this.listCategories = listCategories;
        mDragStartListener = dragStartListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_settings_tab_menu, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {

        int realPosition = holder.getAdapterPosition();
        Category category = listCategories.get(realPosition);
        category.setOrder(realPosition);
        holder.itemView.setTag(category.getTitle());
        holder.checkBox.setChecked(category.isShowOnTabMenu());
        holder.textView.setText(category.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int realPosition = holder.getAdapterPosition();
                Category category = listCategories.get(realPosition);
                boolean isShowOnTabMenu = listCategories.get(realPosition).isShowOnTabMenu();
                category.setShowOnTabMenu(!isShowOnTabMenu);
                notifyItemChanged(realPosition);
            }
        });

        // Start a drag whenever the handle view it touched
        holder.handleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        listCategories.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(listCategories, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return listCategories.size();
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        final CheckBox checkBox;
        final TextView textView;
        final ImageView handleView;

        ItemViewHolder(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.settings_tab_menu_checkbox);
            textView = itemView.findViewById(R.id.settings_tab_menu_text);
            handleView = itemView.findViewById(R.id.settings_tab_menu_drag);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
            notifyDataSetChanged();
        }
    }
}

package org.azasoft.FcInterNews.SettingsTabMenu;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.R;
import java.util.ArrayList;
import java.util.HashMap;

public class SettingsTabMenuUtility {

    public static void parseTabsFromString(String savedTabsString, HashMap<Integer, Category> hashMapCategories, ArrayList<Category> listCategories) {
        String[] savedTabsStringArray = savedTabsString.split(",");

        for (String element : savedTabsStringArray) {
            String elementTrim = element.trim();
            Category currentCategory;
            int idElement;
            char firstChar = elementTrim.charAt(0);
            if (firstChar == '#') {
                idElement = Integer.parseInt(elementTrim.substring(1));
                currentCategory = hashMapCategories.get(idElement);
                currentCategory.setShowOnTabMenu(true);
            } else {
                idElement = Integer.parseInt(elementTrim);
                currentCategory = hashMapCategories.get(idElement);
                currentCategory.setShowOnTabMenu(false);
            }
            listCategories.add(hashMapCategories.get(idElement));
        }
    }

    public static void selectItemOnTabMenu(Category category, Activity activity, TabLayout.OnTabSelectedListener tabSelectedListener) {
        // select item on tab menu layout
        TabLayout tabLayout = activity.findViewById(R.id.tabs);

        int numTabPresent = tabLayout.getTabCount();

        for (int i = 0; i < numTabPresent; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                if ((int) tab.getTag() == category.getId()) {
                    focusTabOnTabLayout(tabLayout, tab, tabSelectedListener);
                    return;
                }
            }
        }
        // create tab if not exist
        TabLayout.Tab currentTab = tabLayout.newTab();
        currentTab.setTag(category.getId());
        currentTab.setText(category.getTitle());
        tabLayout.addTab(currentTab, true);
        focusTabOnTabLayout(tabLayout, currentTab, tabSelectedListener);
    }

    private static void focusTabOnTabLayout(final TabLayout tabLayout, final TabLayout.Tab tab, final TabLayout.OnTabSelectedListener tabSelectedListener) {
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tab.select();
                if (tabSelectedListener != null) {
                    tabLayout.addOnTabSelectedListener(tabSelectedListener);
                }
            }
        });
    }
}

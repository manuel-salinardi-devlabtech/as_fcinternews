package org.azasoft.FcInterNews.SettingsTabMenu;

import android.support.v7.widget.RecyclerView;

public interface OnEndDragListener {
    void onEndDrag(RecyclerView.ViewHolder viewHolder);
}

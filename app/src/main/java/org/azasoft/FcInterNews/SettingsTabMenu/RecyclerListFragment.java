package org.azasoft.FcInterNews.SettingsTabMenu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.Models.Categories;
import org.azasoft.FcInterNews.Models.Category;
import org.azasoft.FcInterNews.R;

import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerListFragment extends Fragment implements OnStartDragListener {

    private ItemTouchHelper mItemTouchHelper;
    RecyclerView recyclerView;
    Activity activity;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Category> listCategories = new ArrayList<>();
    HashMap<Integer, Category> hashMapCategories =  Categories.HASH_CATEGORIES;
    String PREF_TAB_MENU;

    public RecyclerListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings_tab_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PREF_TAB_MENU = getResources().getString(R.string.pref_tab_menu);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        String savedTabsString = sharedPref.getString(PREF_TAB_MENU, "0");


        if (!savedTabsString.equals("0")) {
            SettingsTabMenuUtility.parseTabsFromString(savedTabsString, hashMapCategories, listCategories);
        } else {
            listCategories = (ArrayList<Category>) Categories.LIST_CATEGORIES.clone();
            listCategories.remove(0); // remove store online item
            listCategories.remove(0); // remove all news item
        }

        RecyclerListAdapter adapter = new RecyclerListAdapter(listCategories, this);

        recyclerView = view.findViewById(R.id.rv_settings_tab_menu);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        // divider line item
        if (recyclerView.getItemDecorationCount() == 0) {
            RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
            recyclerView.addItemDecoration(itemDecoration);
        }

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onPause() {
        super.onPause();

        ArrayList<String> itemsIdList = new ArrayList<>();

        // make list of the id
        for (Category category : listCategories) {
            String categoryId = String.valueOf(category.getId());

            // if the checkbox is checked put # before the category id
            if (category.isShowOnTabMenu()) {
                itemsIdList.add(("#" + categoryId).trim());
            } else {
                itemsIdList.add(categoryId.trim());
            }
        }
        // stringify list
        String stringForArray = itemsIdList.toString();
        // remove first '[' and last ']' character from the string
        String stringForArrayFormatted = stringForArray.substring(1, stringForArray.length()-1);

        // save the generated string
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        SharedPreferences.Editor e = sharedPref.edit();
        e.remove(PREF_TAB_MENU);
        e.putString(PREF_TAB_MENU,stringForArrayFormatted);
        e.apply();
    }
}

package org.azasoft.FcInterNews.SettingsTabMenu;

import android.support.design.widget.TabLayout;
import android.widget.LinearLayout;

public class TabLayoutUtility {

    public static void disableClick(TabLayout tabLayout) {
        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
    }

    public static void enableClick(TabLayout tabLayout) {
        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        tabStrip.setEnabled(true);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(true);
        }
    }

}

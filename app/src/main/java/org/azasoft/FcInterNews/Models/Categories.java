package org.azasoft.FcInterNews.Models;

import android.support.v4.content.ContextCompat;

import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Categories {

    public static final ArrayList<Category> LIST_CATEGORIES = new ArrayList<>();
    public static final HashMap<Integer, Category> HASH_CATEGORIES = new HashMap<>();
    private static final HashMap<String,String> colorsCategories = new HashMap<>();

    // TITLES
    private static final String STORE_ONLINE =  App.getContext().getString(R.string.tab_store_online);
    private static final String ALL_NEWS =  App.getContext().getString(R.string.tab_all_news);
    private static final String IN_PRIMO_PIANO =  App.getContext().getString(R.string.tab_in_primo_piano);
    private static final String FOCUS =  App.getContext().getString(R.string.tab_focus);
    private static final String NEWS =  App.getContext().getString(R.string.tab_news);
    private static final String COPERTINA =  App.getContext().getString(R.string.tab_copertina);
    private static final String DA_APPIANO =  App.getContext().getString(R.string.tab_da_appiano);
    private static final String PAGELLE =  App.getContext().getString(R.string.tab_pagelle);
    private static final String EDITORIALE =  App.getContext().getString(R.string.tab_editoriale);
    private static final String EX_NERAZZURRI =  App.getContext().getString(R.string.tab_ex_nerazzurri);
    private static final String VISTI_DA_VOI =  App.getContext().getString(R.string.tab_visti_da_voi);
    private static final String WEB_TV =  App.getContext().getString(R.string.tab_web_tv);
    private static final String GIOVANILI =  App.getContext().getString(R.string.tab_giovanili);
    private static final String DA_ZERO_A_DIECI =  App.getContext().getString(R.string.tab_da_zero_a_dieci);
    private static final String CURIOSITA_E_GOSSIP =  App.getContext().getString(R.string.tab_curiosita_e_gossip);
    private static final String AVVERSARIO =  App.getContext().getString(R.string.tab_avversario);
    private static final String LA_MEGLIO_GIOVENTU =  App.getContext().getString(R.string.tab_la_meglio_gioventu);
    private static final String INTER_SOCIAL_CLUB =  App.getContext().getString(R.string.tab_inter_social_club);
    private static final String MULTIMEDIA =  App.getContext().getString(R.string.tab_multimedia);
    private static final String RASSEGNA_STAMPA =  App.getContext().getString(R.string.tab_rassegna_stampa);

    // LINKS
    private static final String LINK_STORE_ONLINE =  App.getContext().getString(R.string.link_store_online);
    private static final String LINK_ALL_NEWS =  App.getContext().getString(R.string.link_all_news);
    private static final String LINK_IN_PRIMO_PIANO =  App.getContext().getString(R.string.link_in_primo_piano);
    private static final String LINK_FOCUS =  App.getContext().getString(R.string.link_focus);
    private static final String LINK_NEWS =  App.getContext().getString(R.string.link_news);
    private static final String LINK_COPERTINA =  App.getContext().getString(R.string.link_copertina);
    private static final String LINK_DA_APPIANO =  App.getContext().getString(R.string.link_da_appiano);
    private static final String LINK_PAGELLE =  App.getContext().getString(R.string.link_pagelle);
    private static final String LINK_EDITORIALE =  App.getContext().getString(R.string.link_editoriale);
    private static final String LINK_EX_NERAZZURRI =  App.getContext().getString(R.string.link_ex_nerazzurri);
    private static final String LINK_VISTI_DA_VOI =  App.getContext().getString(R.string.link_visti_da_voi);
    private static final String LINK_WEB_TV =  App.getContext().getString(R.string.link_web_tv);
    private static final String LINK_GIOVANILI =  App.getContext().getString(R.string.link_giovanili);
    private static final String LINK_DA_ZERO_A_DIECI =  App.getContext().getString(R.string.link_da_zero_a_dieci);
    private static final String LINK_CURIOSITA_E_GOSSIP =  App.getContext().getString(R.string.link_curiosita_e_gossip);
    private static final String LINK_AVVERSARIO =  App.getContext().getString(R.string.link_avversario);
    private static final String LINK_LA_MEGLIO_GIOVENTU =  App.getContext().getString(R.string.link_la_meglio_gioventu);
    private static final String LINK_INTER_SOCIAL_CLUB =  App.getContext().getString(R.string.link_inter_social_club);
    private static final String LINK_MULTIMEDIA =  App.getContext().getString(R.string.link_multimedia);
    private static final String LINK_RASSEGNA_STAMPA =  App.getContext().getString(R.string.link_rassegna_stampa);

    // COLORS
    private static final String COLOR_DEFAULT =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(), R.color.colorAccent) & 0x00ffffff);
    private static final String COLOR_IN_PRIMO_PIANO =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(), R.color.color_in_primo_piano) & 0x00ffffff);
    private static final String COLOR_FOCUS =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_focus) & 0x00ffffff);
    private static final String COLOR_NEWS =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_news) & 0x00ffffff);
    private static final String COLOR_COPERTINA =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_copertina) & 0x00ffffff);
    private static final String COLOR_DA_APPIANO =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_da_appiano) & 0x00ffffff);
    private static final String COLOR_PAGELLE =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_pagelle) & 0x00ffffff);
    private static final String COLOR_EDITORIALE =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_editoriale) & 0x00ffffff);
    private static final String COLOR_EX_NERAZZURRI =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_ex_nerazzurri) & 0x00ffffff);
    private static final String COLOR_VISTI_DA_VOI =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_visti_da_voi) & 0x00ffffff);
    private static final String COLOR_WEB_TV =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_web_tv) & 0x00ffffff);
    private static final String COLOR_GIOVANILI =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_giovanili) & 0x00ffffff);
    private static final String COLOR_DA_ZERO_A_DIECI =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_da_zero_a_dieci) & 0x00ffffff);
    private static final String COLOR_CURIOSITA_E_GOSSIP =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_curiosita_e_gossip) & 0x00ffffff);
    private static final String COLOR_AVVERSARIO =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_avversario) & 0x00ffffff);
    private static final String COLOR_LA_MEGLIO_GIOVENTU =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_la_meglio_gioventu) & 0x00ffffff);
    private static final String COLOR_INTER_SOCIAL_CLUB =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_inter_social_club) & 0x00ffffff);
    private static final String COLOR_MULTIMEDIA =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_multimedia) & 0x00ffffff);
    private static final String COLOR_RASSEGNA_STAMPA =  "#" + Integer.toHexString(ContextCompat.getColor(App.getContext(),R.color.color_rassegna_stampa) & 0x00ffffff);

    static {
        LIST_CATEGORIES.add(new Category(1, STORE_ONLINE, LINK_STORE_ONLINE, true));
        LIST_CATEGORIES.add(new Category(2, ALL_NEWS, LINK_ALL_NEWS));
        LIST_CATEGORIES.add(new Category(3, IN_PRIMO_PIANO, LINK_IN_PRIMO_PIANO));
        LIST_CATEGORIES.add(new Category(4, FOCUS, LINK_FOCUS));
        LIST_CATEGORIES.add(new Category(5, NEWS, LINK_NEWS));
        LIST_CATEGORIES.add(new Category(6, COPERTINA, LINK_COPERTINA));
        LIST_CATEGORIES.add(new Category(7, DA_APPIANO, LINK_DA_APPIANO));
        LIST_CATEGORIES.add(new Category(8, PAGELLE, LINK_PAGELLE));
        LIST_CATEGORIES.add(new Category(9, EDITORIALE, LINK_EDITORIALE));
        LIST_CATEGORIES.add(new Category(10, EX_NERAZZURRI, LINK_EX_NERAZZURRI));
        LIST_CATEGORIES.add(new Category(11, VISTI_DA_VOI, LINK_VISTI_DA_VOI));
        LIST_CATEGORIES.add(new Category(12, WEB_TV, LINK_WEB_TV));
        LIST_CATEGORIES.add(new Category(13, GIOVANILI, LINK_GIOVANILI));
        LIST_CATEGORIES.add(new Category(14, DA_ZERO_A_DIECI, LINK_DA_ZERO_A_DIECI));
        LIST_CATEGORIES.add(new Category(15, CURIOSITA_E_GOSSIP, LINK_CURIOSITA_E_GOSSIP));
        LIST_CATEGORIES.add(new Category(16, AVVERSARIO, LINK_AVVERSARIO));
        LIST_CATEGORIES.add(new Category(17, LA_MEGLIO_GIOVENTU, LINK_LA_MEGLIO_GIOVENTU));
        LIST_CATEGORIES.add(new Category(18, INTER_SOCIAL_CLUB, LINK_INTER_SOCIAL_CLUB));
        LIST_CATEGORIES.add(new Category(19, MULTIMEDIA, LINK_MULTIMEDIA));
        LIST_CATEGORIES.add(new Category(20, RASSEGNA_STAMPA, LINK_RASSEGNA_STAMPA));

        colorsCategories.put(IN_PRIMO_PIANO, COLOR_IN_PRIMO_PIANO);
        colorsCategories.put(FOCUS, COLOR_FOCUS);
        colorsCategories.put(NEWS, COLOR_NEWS);
        colorsCategories.put(COPERTINA, COLOR_COPERTINA);
        colorsCategories.put(DA_APPIANO, COLOR_DA_APPIANO);
        colorsCategories.put(PAGELLE, COLOR_PAGELLE);
        colorsCategories.put(EDITORIALE, COLOR_EDITORIALE);
        colorsCategories.put(EX_NERAZZURRI, COLOR_EX_NERAZZURRI);
        colorsCategories.put(VISTI_DA_VOI, COLOR_VISTI_DA_VOI);
        colorsCategories.put(WEB_TV, COLOR_WEB_TV);
        colorsCategories.put(GIOVANILI, COLOR_GIOVANILI);
        colorsCategories.put(DA_ZERO_A_DIECI, COLOR_DA_ZERO_A_DIECI);
        colorsCategories.put(CURIOSITA_E_GOSSIP, COLOR_CURIOSITA_E_GOSSIP);
        colorsCategories.put(AVVERSARIO, COLOR_AVVERSARIO);
        colorsCategories.put(LA_MEGLIO_GIOVENTU, COLOR_LA_MEGLIO_GIOVENTU);
        colorsCategories.put(INTER_SOCIAL_CLUB, COLOR_INTER_SOCIAL_CLUB);
        colorsCategories.put(MULTIMEDIA, COLOR_MULTIMEDIA);
        colorsCategories.put(RASSEGNA_STAMPA, COLOR_RASSEGNA_STAMPA);
    }

    static {
        HASH_CATEGORIES.put(1,new Category(1, STORE_ONLINE, LINK_STORE_ONLINE, true));
        HASH_CATEGORIES.put(2,new Category(2, ALL_NEWS, LINK_ALL_NEWS));
        HASH_CATEGORIES.put(3,new Category(3, IN_PRIMO_PIANO, LINK_IN_PRIMO_PIANO));
        HASH_CATEGORIES.put(4,new Category(4, FOCUS, LINK_FOCUS));
        HASH_CATEGORIES.put(5,new Category(5, NEWS, LINK_NEWS));
        HASH_CATEGORIES.put(6,new Category(6, COPERTINA, LINK_COPERTINA));
        HASH_CATEGORIES.put(7,new Category(7, DA_APPIANO, LINK_DA_APPIANO));
        HASH_CATEGORIES.put(8,new Category(8, PAGELLE, LINK_PAGELLE));
        HASH_CATEGORIES.put(9,new Category(9, EDITORIALE, LINK_EDITORIALE));
        HASH_CATEGORIES.put(10,new Category(10, EX_NERAZZURRI, LINK_EX_NERAZZURRI));
        HASH_CATEGORIES.put(11,new Category(11, VISTI_DA_VOI, LINK_VISTI_DA_VOI));
        HASH_CATEGORIES.put(12,new Category(12, WEB_TV, LINK_WEB_TV));
        HASH_CATEGORIES.put(13,new Category(13, GIOVANILI, LINK_GIOVANILI));
        HASH_CATEGORIES.put(14,new Category(14, DA_ZERO_A_DIECI, LINK_DA_ZERO_A_DIECI));
        HASH_CATEGORIES.put(15,new Category(15, CURIOSITA_E_GOSSIP, LINK_CURIOSITA_E_GOSSIP));
        HASH_CATEGORIES.put(16,new Category(16, AVVERSARIO, LINK_AVVERSARIO));
        HASH_CATEGORIES.put(17,new Category(17, LA_MEGLIO_GIOVENTU, LINK_LA_MEGLIO_GIOVENTU));
        HASH_CATEGORIES.put(18,new Category(18, INTER_SOCIAL_CLUB, LINK_INTER_SOCIAL_CLUB));
        HASH_CATEGORIES.put(19,new Category(19, MULTIMEDIA, LINK_MULTIMEDIA));
        HASH_CATEGORIES.put(20,new Category(20, RASSEGNA_STAMPA, LINK_RASSEGNA_STAMPA));
    }



    public static String getCategoryColor(String category) {
        String searchedColor = colorsCategories.get(category);
        if (searchedColor == null) {
            searchedColor = COLOR_DEFAULT;
        }
        return searchedColor;
    }
}

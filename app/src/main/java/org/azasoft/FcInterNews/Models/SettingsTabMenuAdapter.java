package org.azasoft.FcInterNews.Models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.azasoft.FcInterNews.R;

import java.util.ArrayList;

public class SettingsTabMenuAdapter extends RecyclerView.Adapter<SettingsTabMenuAdapter.MyViewHolder> {

    // ---------------- View Holder -------------------------
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textCategory;

        MyViewHolder(View itemView) {
            super(itemView);
            textCategory = itemView.findViewById(R.id.settings_tab_menu_text);
        }
    }

    private Context context;
    private ArrayList<Category> list;
    private LayoutInflater inflater;

    public SettingsTabMenuAdapter(Context appContext, ArrayList<Category> list) {
        this.context = appContext;
        this.list = list;
        this.inflater = (LayoutInflater.from(appContext));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_settings_tab_menu, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textCategory.setText(list.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, list.get(position).getTitle(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

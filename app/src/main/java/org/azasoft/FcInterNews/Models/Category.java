package org.azasoft.FcInterNews.Models;

public class Category {
    private int id;
    private int order = -1;
    private String title;
    private String link;
    private boolean isForBrowser;
    private boolean isShowOnTabMenu = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isForBrowser() {
        return isForBrowser;
    }

    public void setForBrowser(boolean forBrowser) {
        isForBrowser = forBrowser;
    }

    public boolean isShowOnTabMenu() {
        return isShowOnTabMenu;
    }

    public void setShowOnTabMenu(boolean showOnTabMenu) {
        isShowOnTabMenu = showOnTabMenu;
    }

    public Category (int id, String title, String link) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.isForBrowser = false;
    }
    public Category(int id, String title, String link, boolean isForBrowser) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.isForBrowser = isForBrowser;
    }

    public String toString() {
        return title;
    }
    
}

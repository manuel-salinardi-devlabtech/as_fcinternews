package org.azasoft.FcInterNews.Models;

import org.azasoft.FcInterNews.RSS.YoutubeLink;

import java.util.List;

public class Detail {

	private String category ="";
	private String image ="";
	private String text = "";
	private List<YoutubeLink> linkYoutube;
	private String author ="";
	private String lecture ="";
	private String title = "";
	private String date = "";
	private static String shareUrl = "";

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) { 
		this.category = category; 
	}
	public void setImage(String image) {
		this.image = image;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getImage() {
		return image;
	}
	public String getText() {
		return text;
	}
	public List<YoutubeLink> getLinkYoutube() {
		return linkYoutube;
	}
	public void setLinkYoutube(List<YoutubeLink> links) {
		this.linkYoutube = links;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getLecture() {
		return this.lecture;
	}
	public void setLecture(String lecture) {
		this.lecture = lecture;
	}
	public String getTitle() { 
		return this.title; 
	}
	public void setTitle(String title) { 
		this.title = title; 
	}
	public String getDate() { 
		return this.date; 
	}
	public void setDate(String date) {
		this.date = date; 
	}
	public static String getShareUrl() { 
		return Detail.shareUrl; 
	}
	public static void setShareUrl(String shareUrl) { 
		Detail.shareUrl = shareUrl; 
	}

}



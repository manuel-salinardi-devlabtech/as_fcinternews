package org.azasoft.FcInterNews.Models;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartadserver.android.library.model.SASAdElement;
import com.smartadserver.android.library.ui.SASAdView;

import org.azasoft.FcInterNews.AdViewWrapper;
import org.azasoft.FcInterNews.App;
import org.azasoft.FcInterNews.R;
import org.azasoft.FcInterNews.RSS.RSSItem;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RSSItem> mItems;
    private Context mContext;
    private View.OnClickListener mListener;

    private static final int VIEW_TYPE_FIRST_ITEM = 1;
    private static final int VIEW_TYPE_STANDARD = 2;
    private static final int VIEW_TYPE_AD = 3;

    //private ArrayList<RSSItem> mItems;

    public NewsAdapter (Context context, ArrayList<RSSItem> list) {
        this.mItems = list;
    }

    public NewsAdapter(List<RSSItem> items, Context context, View.OnClickListener listener) {
        this.mItems = items;
        this.mContext = context;
        this.mListener = listener;
    }

    // ----------- FIRST ITEM LIST VIEWHOLDER ------------
    class ViewHolderFirstItem extends RecyclerView.ViewHolder {

        TextView firstRowTitle;
        TextView firstRowLabel;
        TextView firstRowDateDay;
        TextView firstRowDateHour;
        ImageView firstRowImg;

        ViewHolderFirstItem(View itemView) {
            super(itemView);

            firstRowTitle = itemView.findViewById(R.id.first_row_title);
            firstRowLabel = itemView.findViewById(R.id.first_row_label);
            firstRowDateDay = itemView.findViewById(R.id.first_row_date_day);
            firstRowDateHour = itemView.findViewById(R.id.first_row_date_hour);
            firstRowImg = itemView.findViewById(R.id.first_row_img);
        }
    }

    // ----------- STANDARD VIEWHOLDER ------------
     class ViewHolderStandard extends RecyclerView.ViewHolder {

        TextView rowTitle;
        TextView rowLabel;
        TextView rowDateDay;
        TextView rowDateHour;
        ImageView rowImg;


        ViewHolderStandard(View itemView) {
            super(itemView);

            rowTitle = itemView.findViewById(R.id.row_title);
            rowLabel = itemView.findViewById(R.id.row_label);
            rowDateDay = itemView.findViewById(R.id.row_date_day);
            rowDateHour = itemView.findViewById(R.id.row_date_hour);
            rowImg = itemView.findViewById(R.id.row_img);

        }
    }

    // ----------- AD VIEWHOLDER ------------
    public class ADViewHolder extends RecyclerView.ViewHolder {
        ADViewHolder(View itemView) {
            super(itemView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        if (viewType == 1) {
            View view = inflater.inflate(R.layout.row_first_news_layout, viewGroup, false);
            view.setOnClickListener(mListener);
            viewHolder = new ViewHolderFirstItem(view);
        } else if (viewType == 2) {
            View view = inflater.inflate(R.layout.row_news_layout, viewGroup, false);
            view.setOnClickListener(mListener);
            viewHolder = new ViewHolderStandard(view);
        } else {
            View view = inflater.inflate(R.layout.list_banner_holder, viewGroup, false);
            view.setOnClickListener(mListener);
            viewHolder = new ADViewHolder(view);
        }
        return viewHolder;
        //View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_news_layout, viewGroup, false);
        //return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == VIEW_TYPE_FIRST_ITEM) {
            ViewHolderFirstItem vh = (ViewHolderFirstItem) viewHolder;
            configureViewHolderFirstItem(vh, position);
        } else if (viewHolder.getItemViewType() == VIEW_TYPE_STANDARD) {
            ViewHolderStandard vh = (ViewHolderStandard) viewHolder;
            configureViewHolderStandard(vh, position);
        } else if (viewHolder.getItemViewType() == VIEW_TYPE_AD) {
            ADViewHolder vh = (ADViewHolder) viewHolder;
            configureViewHolderAD(vh);
        }
    }



    private void configureViewHolderFirstItem(ViewHolderFirstItem viewHolder, int position) {
        RSSItem item = mItems.get(position);

        String fullDate = item.getPubDate();

        viewHolder.itemView.setTag(position);
        viewHolder.firstRowTitle.setText(item.getTitle());
        viewHolder.firstRowLabel.setText(item.getCategory());
        viewHolder.firstRowDateDay.setText(fullDate.substring(0, fullDate.length()-8));
        viewHolder.firstRowDateHour.setText((fullDate.substring(fullDate.length()-8, fullDate.length()-3)) + " ");

        // change color of label
        String colorLabel = Categories.getCategoryColor(item.getCategory());
        viewHolder.firstRowLabel.getBackground().setColorFilter(Color.parseColor(colorLabel), PorterDuff.Mode.SRC_OVER);

        if (item.getImmagine()!=null) {
            viewHolder.firstRowImg.setImageBitmap(item.getImmagine());
        } else {
            viewHolder.firstRowImg.setImageResource(R.drawable.placeholder_image);

        }
    }

    private void configureViewHolderStandard(ViewHolderStandard viewHolder, int position) {
        RSSItem item = mItems.get(position);

        if (item.getIsAdView()) {
            viewHolder.itemView.setTag(position);
            return;
        }

        String fullDate = item.getPubDate();

        viewHolder.itemView.setTag(position);
        viewHolder.rowTitle.setText(item.getTitle());
        viewHolder.rowLabel.setText(item.getCategory());
        viewHolder.rowDateDay.setText(fullDate.substring(0, fullDate.length()-8));
        viewHolder.rowDateHour.setText((fullDate.substring(fullDate.length()-8, fullDate.length()-3)) + " ");

        // change color of label
        String colorLabel = Categories.getCategoryColor(item.getCategory());
        viewHolder.rowLabel.getBackground().setColorFilter(Color.parseColor(colorLabel), PorterDuff.Mode.SRC_OVER);

        if (item.getImmagine()!=null) {
            RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(mContext.getResources(), item.getImmagine());
            dr.setCornerRadius(Math.max(item.getImmagine().getWidth(), item.getImmagine().getHeight()) / 2.0f);
            //dr.setCornerRadius(500.0f);
            viewHolder.rowImg.setImageDrawable(dr);
            //viewHolder.rowImg.setImageBitmap(item.getImmagine());
        } else {
            viewHolder.rowImg.setImageResource(R.drawable.placeholder_image);
        }
    }

    private void configureViewHolderAD(ADViewHolder viewHolder) {

        if (App.isPlayingADS) return;
        
        final AdViewWrapper bannerWrapper = new AdViewWrapper(mContext);

        bannerWrapper.loadAd(mContext.getString(R.string.ad_server_base_url),235558, "935188", 64623, "");


        // To avoid having the parallax going below the actionBar, we have to set the parallax margin top, which is its top limit.
        // To do so, first we get the action bar height
        int actionBarHeight = 0;
        final TypedArray styledAttributes = mContext.getTheme().obtainStyledAttributes(
                new int[] { android.R.attr.actionBarSize }
        );
        actionBarHeight = (int) styledAttributes.getDimension(0, 0);
        // Then we set its height as parallaxMarginTop.
        bannerWrapper.mBanner.setParallaxMarginTop(actionBarHeight);

        if (bannerWrapper != null && bannerWrapper.isAvailable()) { //To be available a wrapper must not have a ViewHolder.
            bannerWrapper.setHolder(viewHolder);
        }
        viewHolder.itemView.setTag(-1);
    }

    @Override
    public int getItemViewType(int position) {
        RSSItem currentItem = mItems.get(position);
        if (currentItem.getIsBigNews()) {
            return VIEW_TYPE_FIRST_ITEM;
        } else if (currentItem.getIsAdView()) {
            return VIEW_TYPE_AD;
        } else {
            return VIEW_TYPE_STANDARD;
        }
    }

    public RSSItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
